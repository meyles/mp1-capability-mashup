### What is this repository for? ###

This project is for practicing React, and displaying Qlik Capability API objects on the front end.

### How do I get set up? ###

#### Summary of set up ####

1) Copy "Beginner's tutorial.qvf" from the data folder into your User> Documents> Qlik> Sense> Apps folder

2) npm install to get the required dependencies for the project

3) Log in to Qlik Desktop and open the Beginner's tutorial app

4) If necessary, adjust the settings in src> utils> qlik-config.js to point to your localhost/port where Qlik is running

#### Dependencies ####

node/npm, Qlik Sense Desktop

