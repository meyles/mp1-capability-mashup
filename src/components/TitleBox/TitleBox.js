import React from "react";
import "./titleBox.css";

function TitleBox() {

    return (
        <div className="titlebox">
            <img 
                className="titlebox-logo"
                alt="Footsie 9000 logo"
                src="./logo.svg"
            />
            <h1 className="titlebox-text">FOOTSIE <span className="titlebox-emphasis-text">9000</span></h1>
        </div>
    )
}

export default TitleBox;