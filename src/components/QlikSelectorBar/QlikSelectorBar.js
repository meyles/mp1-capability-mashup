import React from "react";

function QlikSelectorBar(props) {

    if (props.qlikApp) {
        props.qlikApp.getObject('qlikSelectorBar','CurrentSelections');
    }

    return (
        <div id="qlikSelectorBar" className="qvobject"></div>
    )
}
export default QlikSelectorBar;