import React, { useEffect, useState } from "react";
import useQlikConnection from "../../utils/useQlikConnection";
import ChartGridContainer from "../ChartGridContainer/ChartGridContainer";
import QlikSelectorBar from "../QlikSelectorBar/QlikSelectorBar";
import QlikChart from "../QlikChart/QlikChart";

function QlikDashboard() {
    const qlikApp = useQlikConnection();
    const [chartComponents, setChartComponents] = useState([]);
    const chartObjectIds = [
        "xrGW",
        "JQtPpt",
        "qPyMBp",
        "YdkEMcj",
    ];

    useEffect(()=> {
        const qlikCharts = chartObjectIds.map((objectId, index) => {
            return <QlikChart id={`QV0${index + 1}`} />
        });
        setChartComponents(qlikCharts);
    }, [])

    useEffect(()=> {
        if(qlikApp) {
            chartObjectIds.forEach((objectId, index) => {
                qlikApp.getObject(`QV0${index + 1}`, objectId);
            })
        }
    }, [qlikApp])

    return (
        <div className="qlikDashboard">
            <QlikSelectorBar qlikApp={qlikApp} />
            <ChartGridContainer charts={chartComponents} />
        </div>
    )
}

export default QlikDashboard;