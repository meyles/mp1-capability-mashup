import React from "react";
import QlikDashboard from "../QlikDashboard/QlikDashboard";

function SalesDashboardPage() {

    return (
        <div className="page-container">
            <div className="center-container">
                <h2>Sales Dashboard</h2>
                <QlikDashboard />
            </div>
        </div>
    )
}

export default SalesDashboardPage;