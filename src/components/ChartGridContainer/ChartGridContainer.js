import React, { useEffect, useState, useRef } from "react";
import "./chartGridContainer.css";

function ChartGridContainer(props) {
    const chartContainer = useRef();
    const [chartContainerHeight, setChartContainerHeight] = useState(20);

    function calculateRemainingWindowHeight(elem) {
        const topDistance = elem.getBoundingClientRect().top;
        return window.innerHeight - topDistance - 5;
    }

    useEffect(()=> {
        const height = calculateRemainingWindowHeight(chartContainer.current);
        setChartContainerHeight(height);

        function handleResize() {
            setChartContainerHeight(calculateRemainingWindowHeight(chartContainer.current));
        }

        window.addEventListener('resize', handleResize);

        return ()=> {
            window.removeEventListener('resize', handleResize);
        }
    }, [])

    return (
        <div className="chart-grid" ref={chartContainer} style={{height: chartContainerHeight}}>
            <div id="gridChart1">{props.charts[0]}</div>
            <div id="gridChart2">{props.charts[1]}</div>
            <div id="gridChart3">{props.charts[2]}</div>
            <div id="gridChart4">{props.charts[3]}</div>
        </div>
    )
}

export default ChartGridContainer;