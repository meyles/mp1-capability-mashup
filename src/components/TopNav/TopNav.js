import React from "react";
import "./topNav.css";

function TopNav() {

    return (
        <nav className="topnav">
            <a className="topnav-link" href="#">Investments</a>
            <a className="topnav-link" href="#">Capital</a>
            <a className="topnav-link" href="#">Dashboards</a>
        </nav>
    )
}

export default TopNav;
