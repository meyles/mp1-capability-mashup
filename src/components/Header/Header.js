import React from "react";
import "./header.css";
import TitleBox from "../TitleBox/TitleBox";
import TopNav from "../TopNav/TopNav";

function Header() {

    return (
        <div className="header-background">
            <div className="center-container">
                <header>
                    <TitleBox />
                    <TopNav />
                </header>
            </div>
        </div>
    )
}

export default Header;
