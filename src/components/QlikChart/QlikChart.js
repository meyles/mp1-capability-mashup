import React from "react";

function QlikChart(props) {
    return (
        <div id={props.id} className="qvobject" style={{height:"100%", width:"100%"}}></div>
    )
}

export default QlikChart;