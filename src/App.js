import React from 'react';
import SalesDashboardPage from './components/SalesDashboardPage/SalesDashboardPage';
import Header from './components/Header/Header';

function App() {
  return (
    <div className="App">
      <Header />
      <SalesDashboardPage />
    </div>
  );
}

export default App;
