const config = {
    appId: "Beginner's tutorial.qvf",
    host: "localhost", //the address of your Qlik Engine Instance
    prefix: "", //or the virtual proxy to be used. for example "/anonymous/"
    port: 4848, //or the port to be used if different from the default port  
    isSecure: false
};

export default config;