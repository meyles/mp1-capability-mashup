import { useState, useEffect } from "react";
import config from "./qlik-config";
import qlikApp from "./qlikApp";

function useQlikConnection() {
    const [app, setApp] = useState(undefined);

    useEffect( () => {
            qlikApp(config).then((qlikObjects)=> {
                setApp(qlikObjects.app);
            });
    }, []);

    return app;
}

export default useQlikConnection;